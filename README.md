# Karboout

Prosty user script do usprawniania strony głównej purepc.pl - daje możliwość przeniesienia wyżej tekstów ulubionych autorów.

Testowany w tampermonkey w operze - w innych przeglądarkach opartych na chromium też powinien działać.

# Instalacja

W tampermoneky w zakładce narzędzia w install from url wkleić link https://gitlab.com/wrekawek/karboout/-/raw/main/karboout.js i zainstalować.

# Wyniki

przed:

![Strona przed użyciem skryptu](img/przed.png)

po:

![Strona po użyciu skryptu](img/po.png)
