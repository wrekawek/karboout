// ==UserScript==
// @name         Karboout
// @namespace    https://gitlab.com/
// @version      1.2
// @description  Przenoszenie tekstow ulubionych autorow na gore strony
// @author       wrekawek
// @match        https://www.purepc.pl
// @homepage     https://gitlab.com/wrekawek/karboout/-/raw/main/karboout.js
// @supportURL   https://gitlab.com/wrekawek/karboout
// @updateURL    https://gitlab.com/wrekawek/karboout/-/raw/main/karboout.js
// @downloadURL  https://gitlab.com/wrekawek/karboout/-/raw/main/karboout.js
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// @require code.jquery.com/jquery-latest.js
// @run-at        document-idle
// ==/UserScript==

function move() {
    var $moveUp = $( ".ln_item" ).filter(":contains('Oktaba'), :contains('Marusiak'), :contains('Piwowarczyk'), :contains('Akcja partnerska')").detach();
    $(".latest_items:eq(2)").prepend($( ".ln_item" ).detach());
    $(".latest_items:first").prepend($moveUp);
    $( ".container:first" ).prepend($( ".latest_items:first" ).detach());
}

$( document ).ready(move);
